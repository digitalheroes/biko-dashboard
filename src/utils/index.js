import { isBefore, parseISO } from "date-fns";
import emojiUtils from 'node-emoji';

/**
 * Calculates in percent, the change between 2 numbers.
 * e.g from 1000 to 500 = 50%
 *
 * @param oldNumber The initial value
 * @param newNumber The value that changed
 */
export function getPercentageChange(oldNumber, newNumber){
  const decreaseValue = oldNumber - newNumber;

  if (decreaseValue === 0) return 0;

  if (oldNumber > newNumber)
    return -((decreaseValue / oldNumber) * 100);
  return ((decreaseValue / oldNumber) * 100);
}

/**
 * Sort messages from old to new
 * @param array of messages to be sorted
 * @return array
 */
export const sortMessagesByDate = (array) => {
  return array.sort((a, b) =>
    isBefore(parseISO(a.date), parseISO(b.date)) ? -1 : 1,
  );
};

/**
 * get the profile stored on localStorage
 * @return {Promise<void>}
 */
export const getEmployer = async () => {
  try {
    const employer = await JSON.parse(localStorage.getItem("userData"));
    return employer
  } catch (error) {
    // Error saving data
    throw error;
  }
};

/**
 *
 * @param content
 * @return {boolean}
 */
export const contentIsOnlyEmoji = (content) => {
  const containsEmoji = emojiUtils.hasEmoji(content);
  const containsNonEmoji = emojiUtils.strip(content).length > 0;


  return containsEmoji && !containsNonEmoji
};
