import { withSnackbar } from "notistack";
import React, { useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { useHistory } from "react-router-dom";

import loginStyle from "../../assets/jss/biko-dashboard/layouts/loginStyle";
import { getCurrentEmployer, login } from "../../services/api";
import Button from "../../components/CustomButtons/Button";
import CustomInput from "../../components/CustomInput";
import GridContainer from "../../components/Grid/GridContainer";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import GridItem from "../../components/Grid/GridItem";
import CardHeader from "../../components/Card/CardHeader";

const Login = (props) => {
  const [user, setUser] = useState({});
  const history = useHistory();
  const _handleChange = element => {
    let { target } = element;
    setUser({
      ...user,
      [target.name]: target.value
    });
  };

  const _handleSubmit = event => {
    event.preventDefault();
  };

  const _storeToken = async user => {
    try {
      return await localStorage.setItem("user", JSON.stringify(user));
    } catch (error) {
      // Error saving data
      throw error;
    }
  };

  const _storeUserData = async user => {
    try {
      return await localStorage.setItem("userData", JSON.stringify(user));
    } catch (error) {
      // Error saving data
      throw error;
    }
  };

  const signIn = async () => {
    try {
      const {token} = await login(user);
      await _storeToken(token);
      const data = await getCurrentEmployer();
      await _storeUserData(data);
      return history.push('/dashboard')
    } catch (error) {
      const {enqueueSnackbar} = props;
      return enqueueSnackbar(error.message, {variant: 'error'});
    }
  };
  if (localStorage.getItem("user")) return history.push("/dashboard");
  return (
    <div>
      <GridContainer justify={"center"}>
        <GridItem xs={12} sm={6} md={6} lg={4}>
          <Card>
            <CardHeader color={"primary"}>
              <h4>Entrar</h4>
            </CardHeader>
            <CardBody>
              <form onSubmit={_handleSubmit} method={"post"}>
                <CustomInput
                  labelText={"Email"}
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    name: "email",
                    type: "email",
                    onChange: _handleChange,
                    defaultValue: user.email,
                    required: true,
                    placeholder: "hector.salamanca@biko.com.br"
                  }}
                />
                <CustomInput
                  labelText="Senha"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    name: "password",
                    type: "password",
                    onChange: _handleChange,
                    required: true,
                    placeholder: "******"
                  }}
                />
                <Button
                  color={"primary"}
                  round
                  onClick={signIn}
                >
                  {"Enviar"}
                </Button>
              </form>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
};

export default withSnackbar(withStyles(loginStyle)(Login));
