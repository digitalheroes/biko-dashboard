/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import { FiActivity, FiBriefcase, FiUser } from "react-icons/fi";
import { Switch, Route, Redirect } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Header from "../../components/Header/Header.jsx";
import Sidebar from "../../components/Sidebar";

import dashboardStyle from "../../assets/jss/biko-dashboard/layouts/dashboardStyle.jsx";

import image from "../../assets/img/cover.jpg";
import logo from "../../assets/img/reactlogo.png";
import CreateJob from "../../views/CreateJob";
import JobDetail from "../../views/JobDetail";
import ApplicationDetail from "../../views/ApplicationDetail";
import JobList from "../../views/JobList";
import UserProfile from "../../views/UserProfile";
import DashboardView from "../../views/Dashboard";

const routes = [
  {
    path: "/",
    sidebarName: "Dashboard",
    navbarName: "Dashboard",
    icon: FiActivity,
    component: DashboardView
  },
  {
    path: "/jobs",
    sidebarName: "Minhas Vagas",
    navbarName: "Vagas",
    icon: FiBriefcase,
    component: JobList
  },
  {
    path: "/user",
    sidebarName: "Perfil",
    navbarName: "Perfil",
    icon: FiUser,
    component: UserProfile
  }
];

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileOpen: false
    };
    this.resizeFunction = this.resizeFunction.bind(this);
  }

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  getRoute() {
    return this.props.location.pathname !== "/maps";
  }
  resizeFunction() {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.resizeFunction);
  }
  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeFunction);
  }
  render() {
    const { classes, ...rest } = this.props;

    if (!localStorage.getItem("user"))
      return <Redirect exact to={"/login"} />;

    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={routes}
          logoText={"Biko Universitário"}
          logo={logo}
          image={image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color="primary"
          {...rest}
        />
        <div className={classes.mainPanel} ref="mainPanel">
          <Header
            routes={routes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}/>
            <div className={classes.content}>
              <Switch>
                <Route exact path="/" component={DashboardView} />
                <Route exact path="/jobs" component={JobList} />
                <Route exact path="/jobs/create" component={CreateJob} />
                <Route exact path="/jobs/:id" component={JobDetail} />
                <Route exact path="/user" component={UserProfile} />
                <Route exact path="/applications/:id" component={ApplicationDetail} />
              </Switch>
            </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
