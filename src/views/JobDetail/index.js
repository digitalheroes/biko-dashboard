import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
// react-icons
import {
  FiClipboard,
  FiClock,
  FiDollarSign,
  FiRotateCcw
} from "react-icons/fi";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Applications from "../../components/ApplicationTable";
import Card from "components/Card/Card.jsx";
import CardHeader from "../../components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import dashboardStyle from "assets/jss/biko-dashboard/views/dashboardStyle.jsx";
import { getJobById } from "../../services/api";

import moment from "moment";
import "moment/locale/pt-br";

class JobDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jobDetail: {
        CompanyId: Number,
        jobTitle: String,
        jobDescription: String,
        jobQuantity: Number,
        jobRemuneration: Number,
        jobPeriod: String,
        jobType: String,
        jobCheckIn: String,
        jobCheckOut: String,
        createdAt: String,
        updatedAt: String,
        Company: Object,
        Applications: []
      }
    };
    this.getJobDetail(props.match.params.id);
  }
  static propTypes = {
    classes: PropTypes.object.isRequired,
    match: PropTypes.object
  };

  getJobDetail(id) {
    return getJobById(id)
      .then(jobDetail => {
        this.setState({ jobDetail });
      })
      .catch(error => {
        throw error;
      });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={9}>
            <Card>
              <CardHeader>
                <p className={classes.cardCategory}>{`ID da vaga #${
                  this.state.jobDetail.id
                }`}</p>
                <h3 className={classes.cardTitle}>
                  {this.state.jobDetail.jobTitle}
                </h3>
              </CardHeader>
              <CardBody>
                <p>{this.state.jobDetail.jobDescription}</p>
                <h6>{`das ${moment(this.state.jobDetail.jobCheckIn).format('HH:mm')} as ${
                  moment(this.state.jobDetail.jobCheckOut).format('HH:mm')
                }`}</h6>
                <h5>
                  R$ {this.state.jobDetail.jobRemuneration}
                </h5>
              </CardBody>
              <CardFooter>
                <div className={classes.stats}>
                  <FiClock />
                  {`Criado ${moment(this.state.jobDetail.createdAt).calendar()}`}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="info" stats icon>
                <CardIcon color="info">
                  <FiClipboard />
                </CardIcon>
                <p className={classes.cardCategory}>{"Candidaturas"}</p>
                <h3 className={classes.cardTitle}>{this.state.jobDetail.Applications.length}</h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <FiRotateCcw />
                  {`Atualizado ${moment(this.state.jobDetail.updatedAt).calendar()}`}
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="gray">
                <h4 className={classes.cardTitleWhite}>Candidaturas</h4>
                <p className={classes.cardCategoryWhite}>
                  {`Última candidatura: Nunca`}
                </p>
              </CardHeader>
              <CardBody>
                <Applications
                  tableData={this.state.jobDetail.Applications}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(dashboardStyle)(JobDetail);
