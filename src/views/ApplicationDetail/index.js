import { formatISO, parseISO } from "date-fns";
import React, { useEffect, useState, useRef } from "react";
import classNames from 'classnames';
import Fab from "@material-ui/core/Fab";
import IconButton from "@material-ui/core/IconButton";
import firebase from "firebase";
import { primaryColor, grayColor } from "assets/jss/biko-dashboard.jsx";
import EmojiPicker, {SKIN_TONE_MEDIUM_DARK} from 'emoji-picker-react';
import ScrollToBottom from 'react-scroll-to-bottom';
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import InputBase from "@material-ui/core/InputBase";
// react-icons
import {
  FiFeather,
  FiClock,
  FiRotateCcw,
  FiSmile
} from "react-icons/fi";
import {RiCloseFill} from "react-icons/ri";
import {MdChatBubble} from "react-icons/md";
import {IoMdSend} from "react-icons/io";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "../../components/Card/CardHeader.jsx";
import ChatBubble from "../../components/ChatBubble";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import dashboardStyle from "../../assets/jss/biko-dashboard/views/dashboardStyle.jsx";
import Button from "../../components/CustomButtons/Button";
import { getApplicationDetail, triggerApplicationNotification } from "../../services/api";

import moment from "moment";
import "moment/locale/pt-br";
import { sortMessagesByDate, getEmployer, contentIsOnlyEmoji } from "../../utils";

const ApplicationDetail = ({ classes, match }) => {
  const chatContainerRef = useRef();
  const { id } = match.params;
  const chatRef = firebase.database().ref(`/applications/${id}/chat`);
  const messageRef = firebase.database().ref(`/applications/${id}/chat/messages`);
  const [chatOpen, setChatOpen] = useState(false);
  const [emojiPickerVisible, setEmojiPickerVisible] = useState(false);
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState({});
  useEffect(() => {
    configureUser();
    getData();
    fetchMessages();
  }, []);

  const configureUser = async () => {
    const storedUser = await getEmployer();
    setUser(storedUser);
  };

  const fetchMessages = async () => {
    chatRef.on('value', (snapshot) => {
      if (snapshot.val()) {
        const {messages: fetchedMessages} = snapshot.val();
        const messageArray = Object.keys(fetchedMessages).map((messageId) => ({
          id: messageId,
          ...fetchedMessages[messageId],
        }));
        const sortedMessages = sortMessagesByDate(messageArray);
        setMessages(sortedMessages);
      }
    });
  };

  const handleInputChange = ({target}) => {
    const {value} = target;
    setMessage(value);
  };

  const onEmojiClick = (event, {emoji}) => {
    setMessage(message + emoji)
  };

  const handleMessageSubmit = () => {
    messageRef.push({
      userId: user.id,
      content: message,
      date: formatISO(Date.now()),
    });
    setMessage('');
    triggerApplicationNotification(user.id, id, message);
  };

  const getData = async () => {
    const data = await getApplicationDetail(id);
    setApplication(data);
  };

  const [application, setApplication] = useState({
    Student: {
      Address: {},
      Habilities: []
    }
  });
  const { applicationMessage,createdAt, Student } = application;
  const { firstName, lastName, email, bioDescription, birthDate, Address, Habilities } = Student;
  const { publicPlace, streetNumber, neighborhood, city, state, zipCode } = Address;
  const fullAddress = `${publicPlace}, ${streetNumber} - ${neighborhood}, ${city} - ${state} - ${zipCode}`;
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={6} md={4}>
          <Card>
            <CardHeader>
              <div className={classes.headerInfo}>
                <div>
                  <p className={classes.cardCategory}>{`ID da candidatura #${id}`}</p>
                  <h3 className={classes.cardTitle}>{firstName} {lastName}</h3>
                  <p>{bioDescription}</p>
                </div>
                <Button color="info" onCLick={() => setChatOpen(true)}>
                  Iniciar Chat
                </Button>
              </div>
            </CardHeader>
            <CardBody>
              <p className={classes.cardCategory}>Mensagem do candidato.</p>
              <p>{applicationMessage}</p>
              <p className={classes.cardCategory}>E-mail</p>
              <p>{email}</p>
              <p className={classes.cardCategory}>Data de nascimento</p>
              <p>{moment(birthDate).calendar()}</p>
            </CardBody>
            <CardFooter>
              <div className={classes.stats}>
                <FiClock />
                {`Se candidatou ${moment(createdAt).calendar()}`}
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={4}>
          <Card>
            <CardHeader>
              <p className={classes.cardCategory}>Endereço</p>
              <p>{fullAddress}</p>
            </CardHeader>
            <CardBody>
            </CardBody>
            <CardFooter>
              <div className={classes.stats}>
                <FiClock />
                {`Criado ${moment(createdAt).calendar()}`}
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={4}>
          <Card>
            <CardHeader color="info" stats icon>
              <CardIcon color="info">
                <FiFeather />
              </CardIcon>
              <p className={classes.cardCategory}>Habilidades</p>
              <h3 className={classes.cardTitle}>{Habilities.length}</h3>
            </CardHeader>
            <CardBody>
              {Habilities.map(({ habilityName }, key)=> (
                <div className={classes.habilityTag} key={key}>{habilityName}</div>
              ))}
            </CardBody>
            <CardFooter stats>
              <div className={classes.stats}>
                <FiRotateCcw />
                {`Atualizado ${moment().calendar()}`}
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      <div className={classes.chatButton}>
        {chatOpen && (
          <Card elevation={2} className={classes.chat}>
            <CardHeader className={classes.chatHeader}>
              <h5><b>{firstName} {lastName}</b></h5>
            </CardHeader>
            <ScrollToBottom ref={chatContainerRef} className={classes.chatBody}>
              <div className={classes.chatContainer}>
                {messages.map(({content, date, userId, ...rest}) => (
                  <ChatBubble message={content} sent={userId === user.id} hour={parseISO(date)} onlyEmoji={contentIsOnlyEmoji(content)} />
                ))}
              </div>
            </ScrollToBottom>
            <CardFooter className={classes.chatFooter}>
              <InputBase placeholder="Enviar mensagem..." value={message} onChange={handleInputChange} />
              <div className={classes.emojiPickerContainer}>
                {emojiPickerVisible && (
                  <div className={classes.emojiPicker}>
                    <EmojiPicker onEmojiClick={onEmojiClick} disableAutoFocus={true} skinTone={SKIN_TONE_MEDIUM_DARK} groupNames={{smileys_people:"PEOPLE"}}/>
                  </div>
                )}
                <IconButton disableFocusRipple size="small" onClick={() => setEmojiPickerVisible(!emojiPickerVisible)}>
                  <FiSmile color={emojiPickerVisible ? primaryColor : grayColor}/>
                </IconButton>
              </div>
              <IconButton disableFocusRipple size="small" disabled={!message} onClick={handleMessageSubmit}>
                <IoMdSend color={message ? primaryColor : grayColor}/>
              </IconButton>
            </CardFooter>
          </Card>
        )}
        <Fab size="large" color="primary" onClick={() => {
          setChatOpen(!chatOpen)
        }}>
          {chatOpen ? (
            <RiCloseFill size={28} color="#ffffff" />
          ) : (
            <MdChatBubble size={28} color="#ffffff" />
          )}
        </Fab>
      </div>
    </div>
  );
};

const style = {
  habilityTag: {
    backgroundColor: primaryColor,
    color: '#FFF',
    display: 'inline-block',
    padding: '5px 12px',
    margin: '5px',
    fontSize: '10px',
    textAlign: 'center',
    fontWeight: 700,
    lineHeight: 1,
    borderRadius: '12px',
    textTransform: 'uppercase',
    verticalAlign: 'baseline',
  },
  chatButton: {
    zIndex: 10000,
    position: 'fixed',
    bottom: 20,
    right: 20,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  chat: {
    borderRadius: 8,
    marginBottom: 15,
  },
  chatHeader: {
    backgroundColor: primaryColor,
    color: '#ffffff',
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8
  },
  chatContainer: {
    padding: 20,
    minHeight: 300,
    minWidth: 300
  },
  chatBody: {
    maxHeight: 350,
    overflow: 'scroll'
  },
  chatFooter: {
    margin: 0,
    padding: 15,
    display: 'flex',
    borderTopColor: '#dedede',
    borderTopWidth: 1,
    borderStyle: 'solid',
    flexDirection: 'space-between'
  },
  emojiPickerContainer: {
    position: 'relative'
  },
  emojiPicker: {
    position: 'absolute',
    bottom: 50,
    right: -20
  }
};

export default withStyles({...dashboardStyle, ...style})(ApplicationDetail);
