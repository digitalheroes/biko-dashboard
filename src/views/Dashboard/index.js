import React, { useEffect, useState } from "react";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
import Chartist from "chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import { FiBriefcase, FiClipboard } from "react-icons/fi";
// @material-ui/icons
import DateRange from "@material-ui/icons/DateRange";
import TrendingUp from "@material-ui/icons/TrendingUp";
import TrendingDown from "@material-ui/icons/TrendingDown";
import TrendingFlat from "@material-ui/icons/TrendingFlat";
import AccessTime from "@material-ui/icons/AccessTime";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import {
  dailySalesChart,
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/biko-dashboard/views/dashboardStyle.jsx";
import { getSummary } from "../../services/api";
import { getPercentageChange } from "../../utils";

const Dashboard = ({ classes }) => {
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    const summary = await getSummary();
    setData(summary);
  };
  const [data, setData] = useState({
    weekApplications: {
      "Dom": 0,
      "Seg": 0,
      "Ter": 0,
      "Qua": 0,
      "Qui": 0,
      "Sex": 0,
      "Sab": 0
    },
    applicationCount: 0,
    jobCount: 0
  });
  const { weekApplications, applicationCount } = data;
  const days = Object.keys(weekApplications);
  const chartData = {
    labels: days,
    series: [days.map((day) => weekApplications[day])]
  };
  const chartOptions = {
    lineSmooth: Chartist.Interpolation.cardinal({
      tension: 0
    }),
      low: 0,
      high: applicationCount * 2, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: {
      top: 0,
        right: 0,
        bottom: 0,
        left: 0
    }
  };
  const percentageChange = getPercentageChange(weekApplications[days[5]], weekApplications[days[6]]);
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="primary" stats icon>
              <CardIcon color="primary">
                <FiClipboard />
              </CardIcon>
              <p className={classes.cardCategory}>Candidaturas</p>
              <h3 className={classes.cardTitle}>
                {data.applicationCount}
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <DateRange />
                Últimos 7d
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="primary" stats icon>
              <CardIcon color="primary">
                <FiBriefcase />
              </CardIcon>
              <p className={classes.cardCategory}>Vagas cadastradas</p>
              <h3 className={classes.cardTitle}>{data.jobCount}</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <DateRange />
                Todo o período
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="success">
              <ChartistGraph
                className="ct-chart"
                data={chartData}
                type="Line"
                options={chartOptions}
                listener={dailySalesChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Candidaturas diárias</h4>
              <p className={classes.cardCategory}>
                {(percentageChange === 0) && (
                  <span>
                    <TrendingFlat className={classes.upArrowCardCategory} /> {percentageChange}%
                  </span>
                )}
                {(percentageChange > 0) && (
                  <span className={classes.successText}>
                    <TrendingUp className={classes.upArrowCardCategory} /> {percentageChange}%
                  </span>
                )}
                {(percentageChange < 0) && (
                  <span className={classes.errorText}>
                    <TrendingDown className={classes.upArrowCardCategory} /> {percentageChange}%
                  </span>
                )}
                <span style={{ marginLeft: 5 }}>
                  candidaturas hoje.
                </span>
              </p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> Atualizado agora
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
};

export default withStyles(dashboardStyle)(Dashboard);
