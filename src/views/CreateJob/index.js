import { Formik } from "formik";
import React, { useState, useEffect, useMemo, useRef } from "react";
import { FormControl, TextField, Select, InputLabel, CircularProgress, withStyles } from "@material-ui/core";
import { FiFeather, FiChevronDown } from "react-icons/fi";
import NumberFormat from 'react-number-format';
import { useHistory } from "react-router";
import { throttle } from "lodash"
// @material-ui/core components
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import { withSnackbar } from "notistack";
import { TimePicker } from '@material-ui/pickers';
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import { date, string, object, number } from "yup";
import { primaryColor } from "../../assets/jss/biko-dashboard";
import CardIcon from "../../components/Card/CardIcon";
import Button from "../../components/CustomButtons/Button";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import { REQUIRED_STATEMENT } from "../../constants";

import { createJob, searchHabilities } from "../../services/api";


const styles = {
  habilityContainer: {
    marginTop: 10,
    marginBottom: 10
  },
  habilityTag: {
    backgroundColor: primaryColor,
    color: '#FFF',
    display: 'inline-block',
    padding: '5px 12px',
    margin: '5px',
    fontSize: '10px',
    textAlign: 'center',
    fontWeight: 700,
    lineHeight: 1,
    borderRadius: '12px',
    textTransform: 'uppercase',
    verticalAlign: 'baseline',
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
};

const filter = createFilterOptions();

const CustomTextField = withStyles({
  root: {
    '& label.MuiInputLabel-root':{
      color: "#AAAAAA !important",
      fontWeight: "400",
      fontSize: "14px",
      lineHeight: "1.42857"
    },
    '& label.Mui-focused': {
      color: '#AAA',
    },
    '& .MuiInput-underline': {
      borderBottomColor: '#D2D2D2 !important',
    },
    '& .MuiInput-underline:before': {
      borderBottomColor: '#D2D2D2 !important',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: primaryColor,
    },
    "& .MuiInput-underline:hover:not($disabled):before,&:before": {
      borderColor: "#D2D2D2 !important",
      borderWidth: "1px !important"
    },
  },
})(TextField);

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      thousandSeparator="."
      decimalSeparator=","
      isNumericString
      prefix="R$"
    />
  );
}

const validationSchema = object({
  jobTitle: string().required(REQUIRED_STATEMENT),
  jobRemuneration: string().required(REQUIRED_STATEMENT),
  jobType: number().required(REQUIRED_STATEMENT),
  jobQuantity: string().required(REQUIRED_STATEMENT),
  jobPeriod: number().required(REQUIRED_STATEMENT),
  jobCheckIn: date().required(REQUIRED_STATEMENT),
  jobCheckOut: date().required(REQUIRED_STATEMENT),
  jobDescription: string().required(REQUIRED_STATEMENT),
});

const CreateJob = ({ classes, ...props }) => {
  const history = useHistory();
  const form = useRef(null);
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState(null);
  const [inputValue, setInputValue] = React.useState('');
  const [options, setOptions] = React.useState([]);
  const [job, setJob] = useState({
    habilities: []
  });
  const { habilities } = job;

  const fetch = useMemo(
    () =>
      throttle(async ({ input }, callback) => {
        setLoading(true);
        const habilities = await searchHabilities(input);
        callback(habilities);
        setLoading(false);
      }, 200),
    [],
  );

  useEffect(() => {
    let active = true;

    if (inputValue === '') {
      setOptions(value ? [value] : []);
      return undefined;
    }

    fetch({ input: inputValue }, (results) => {

      if (active) {
        let newOptions = [];

        if (value) {
          newOptions = [value];
        }
        if (results) {
          newOptions = [...newOptions, ...results.map(({ habilityName, id }) => ({ title: habilityName, inputValue: habilityName, value: id }))];
        }

        setOptions(newOptions);
      }
    });

    return () => {
      active = false;
    };
  }, [value, inputValue, fetch]);

  const newJob = async (values) => {
    const { enqueueSnackbar } = props;
    try {
      await createJob({...values, habilities});
      enqueueSnackbar("Vaga cadastrada com sucesso", { variant: "success" });
      return history.push("/jobs");
    } catch (error) {
      enqueueSnackbar("Falha ao cadastar vaga", { variant: "error" })
    }
  };

  // const handleChange = element => {
  //   let { target } = element;
  //   setJob({
  //     ...job,
  //     [target.name]: target.value
  //   });
  // };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={8}>
        <Card>
          <CardHeader color="gray">
            <h4 className={classes.cardTitleWhite}>{"Cadastrar nova vaga"}</h4>
            <p className={classes.cardCategoryWhite}>
              {"Insira os detalhes da vaga"}
            </p>
          </CardHeader>
          <CardBody>
            <Formik
              initialValues={{
                jobTitle: '',
                jobRemuneration: '',
                jobType: 2,
                jobQuantity: '',
                jobPeriod: '',
                jobCheckIn: null,
                jobCheckOut: null,
                jobDescription: '',
              }}
              onSubmit={newJob}
              validationSchema={validationSchema}
            >
              {({values, errors, touched, handleChange, handleSubmit}) => (
                <form ref={form} onSubmit={handleSubmit} method="post">
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={4}>
                      <TextField
                        label="Título da Vaga"
                        value={values.jobTitle}
                        onChange={handleChange}
                        error={touched.jobTitle ? errors.jobTitle : null}
                        name="jobTitle"
                        fullWidth
                        required
                        InputProps={{
                          placeholder: "Sushiman Temporário",
                        }}
                      />
                    </GridItem>
                    <GridItem xs={6} sm={6} md={4}>
                      <TextField
                        label="Remuneração"
                        value={values.jobRemuneration}
                        error={touched.jobRemuneration && errors.jobRemuneration}
                        onChange={handleChange}
                        name="jobRemuneration"
                        required
                        fullWidth
                        InputProps={{
                          inputComponent: NumberFormatCustom,
                          placeholder: "R$ 2.200,00",
                        }}
                      />
                    </GridItem>
                    <GridItem xs={6} sm={6} md={4}>
                      <FormControl className={classes.formControl} fullWidth disabled required>
                        <InputLabel id="jobType">Tipo de vaga</InputLabel>
                        <Select
                          value={values.jobType}
                          error={touched.jobType && errors.jobType}
                          labelId="jobType"
                          name="jobType"
                          onChange={handleChange}
                          placeholder="Selecione um"
                          IconComponent={FiChevronDown}
                          native
                        >
                          <option aria-label="Nenhum" value="" />
                          <option value={1}>Permanente</option>
                          <option selected value={2}>Esporádico</option>
                        </Select>
                      </FormControl>
                    </GridItem>
                  </GridContainer>
                  <GridContainer>
                    <GridItem xs={6} sm={6} md={4}>
                      <TextField
                        label="Quantidade de vagas"
                        value={values.jobQuantity}
                        error={touched.jobQuantity && errors.jobQuantity}
                        required
                        onChange={handleChange}
                        name="jobQuantity"
                        fullWidth
                        InputProps={{
                          placeholder: "3",
                          type: "number",
                          inputProps: {
                            min: 1
                          }
                        }}
                      />
                    </GridItem>
                    <GridItem xs={6} sm={6} md={4}>
                      <FormControl className={classes.formControl} fullWidth required>
                        <InputLabel id="jobPeriod">Período</InputLabel>
                        <Select
                          labelId="jobPeriod"
                          value={values.jobPeriod}
                          error={touched.jobPeriod && errors.jobPeriod}
                          name="jobPeriod"
                          onChange={handleChange}
                          placeholder="Selecione um"
                          native
                          IconComponent={FiChevronDown}
                        >
                          <option aria-label="Nenhum" value="" />
                          <option value={1}>Meio período</option>
                          <option value={2}>Integral</option>
                          <option value={3}>Noturno</option>
                        </Select>
                      </FormControl>
                    </GridItem>
                    <GridItem xs={6} sm={6} md={2}>
                      <TimePicker
                        cancelLabel="Cancelar"
                        value={values.jobCheckIn}
                        error={touched.jobCheckIn && errors.jobCheckIn}
                        ampm={false}
                        required
                        label="Entrada"
                        placeholder="15:00"
                        onChange={(value) => handleChange({
                          target: {
                            name: 'jobCheckIn',
                            value
                          }})} />
                    </GridItem>
                    <GridItem xs={6} sm={6} md={2}>
                      <TimePicker
                        value={values.jobCheckOut}
                        error={touched.jobCheckOut && errors.jobCheckOut}
                        required
                        cancelLabel="Cancelar"
                        ampm={false}
                        label="Saída"
                        placeholder="23:00"
                        onChange={(value) => handleChange({
                          target: {
                            name: 'jobCheckOut',
                            value
                          }})} />
                    </GridItem>
                  </GridContainer>
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                      <TextField
                        label="Descrição de atividades da vaga"
                        fullWidth
                        onChange={handleChange}
                        rows={5}
                        value={values.jobDescription}
                        error={touched.jobDescription && errors.jobDescription}
                        name="jobDescription"
                        multiline
                        inputProps={{
                          name: 'jobDescription',
                          placeholder:
                            "Sushiman, fará pratos de comida por quilo, servindo 1x por semana.",
                        }}
                      />
                    </GridItem>
                  </GridContainer>
                </form>
              )}
            </Formik>
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={6} md={4}>
        <Card>
          <CardHeader color="info" stats icon>
            <CardIcon color="info">
              <FiFeather />
            </CardIcon>
            <p className={classes.cardCategory}>Habilidades</p>
          </CardHeader>
          <CardBody>
            <Autocomplete
              value={value}
              onChange={(event, newValue) => {
                setOptions(newValue ? [newValue, ...options] : options);
                setValue('');
                if (newValue)
                  setJob({...job, habilities: [...habilities, newValue]});
              }}
              onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
              }}
              filterOptions={(options, params) => {
                const filtered = filter(options, params);

                // Suggest the creation of a new value
                if (params.inputValue !== '') {
                  filtered.push({
                    inputValue: params.inputValue,
                    title: `Adicionar "${params.inputValue}"`,
                  });
                }

                return filtered;
              }}
              selectOnFocus
              clearOnBlur
              handleHomeEndKeys
              loading={loading}
              options={options}
              getOptionLabel={(option) => {
                if (typeof option === 'string') {
                  return option;
                }
                if (option.inputValue) {
                  return option.inputValue;
                }
                return option.title;
              }}
              renderOption={({title}) => title}
              freeSolo
              renderInput={(params) => (
                <CustomTextField
                  {...params}
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loading ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                  label="Adicionar habilidade"
                  placeholder="Pesquise uma habilidade..." />
              )}
            />
            <div style={styles.habilityContainer}>
              {habilities.map(({ inputValue }, key)=> (
                <div style={styles.habilityTag} key={key}>
                  <span>{inputValue}</span>
                </div>
              ))}
            </div>
          </CardBody>
          <CardFooter>
            <Button color="primary" onClick={() => {
              const event = new Event('submit', { cancelable: true });
              form.current.dispatchEvent(event);
            }}>
              Cadastrar vaga
            </Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

export default withSnackbar(withStyles(styles)(CreateJob));
