import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import CustomTable from "../../components/Table";
import Button from "../../components/CustomButtons/Button";

import { getCompanyJobs } from "../../services/api";
import moment from "moment";
import "moment/locale/pt-br";
//
const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const JobList = ({ classes }) => {
  const history = useHistory();
  const [jobs, setJobs] = useState([]);
  useEffect(() => {
    getJobs()
  }, []);

  const getJobs = async () => {
    const data = await getCompanyJobs();
    setJobs(filterJobFields(data));
  };

  const filterJobFields = array => {
    return array.map(obj => {
      return Object.keys(obj)
        .filter(key => {
          return !(key === "jobDescription" || key === "CompanyId" || key === "updatedAt");
        })
        .map(key => {
          if (key === 'jobCheckOut' || key === 'jobCheckIn')
            return moment(obj[key]).format('HH:mm');
          if (key === "createdAt" || key === "updatedAt")
            return moment(obj[key]).calendar();
          return obj[key];
        });
    });
  };
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="gray" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            <div>
              <h4 className={classes.cardTitleWhite}>Minhas vagas</h4>
              <p className={classes.cardCategoryWhite}>Todas as vagas cadastradas</p>
            </div>
            <Link to="/jobs/create">
              <Button color="primary">Cadastrar vaga</Button>
            </Link>
          </CardHeader>
          <CardBody>
            <CustomTable
              tableHeaderColor="primary"
              tableHead={[
                "#",
                "Título",
                "Número de vagas",
                "Remuneração",
                "Período",
                "Tipo de Vaga",
                "Entrada",
                "Saída",
                "Data de criação",
                "Ações"
              ]}
              tableData={jobs}
              onClickRow={(id) => {
                history.push(`/jobs/${id}`)
              }}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

export default withStyles(styles)(JobList);
