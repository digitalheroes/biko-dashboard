import http from "./biko";

export function login(payload) {
  return http
    .post("auth", payload)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error.response.data;
    });
}

export function createJob(job) {
  return http
    .post("jobs", job)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error.response.data;
    });
}

export function getCompanyJobs() {
  return http
    .get(`companies/jobs`)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      throw error.response.data;
    });
}

export function getJobById(JobId) {
  return http
    .get(`jobs/${JobId}`)
    .then(job => {
      return job.data;
    })
    .catch(error => {
      throw error.response.data;
    });
}

export async function getApplicationDetail(id) {
  try {
    const { data } = await http.get(`applications/${id}`);
    return data
  } catch (error) {
    const { response } = error;
    throw response.data;
  }
}

export async function getCurrentEmployer() {
  try {
    const { data } = await http.get('auth');
    return data
  } catch (error) {
    const { response } = error;
    throw response.data;
  }
}

export async function getSummary() {
  try {
    const { data } = await http.get('summary');
    return data
  } catch (error) {
    const { response } = error;
    throw response.data;
  }
}

export async function searchHabilities(query) {
  try {
    const { data } = await http.get(`habilities/search`, {
      params: {
        query
      }
    });
    return data
  } catch (error) {
    const { response } = error;
    throw response.data;
  }
}

export async function triggerApplicationNotification(
  senderUserId,
  applicationId,
  message,
) {
  try {
    const {data} = await http.post(`applications/${applicationId}/notification`, {senderUserId, message})
    return data;
  } catch (error) {
    throw error.response.data;
  }
}
