import axios from "axios";

const getToken = () => {
  const user = JSON.parse(localStorage.getItem("user"));
  return user;
};

/**
 * Biko
 * @type {AxiosInstance}
 */
const biko = axios.create({
  baseURL: "https://biko-app.herokuapp.com/api/",
  // baseURL: "http://localhost:5622/api/",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json"
  }
});

biko.interceptors.request.use(
  request => {
    const token = getToken();
    if (token) request.headers.Authorization = `Bearer ${token}`;
    return Promise.resolve(request);
  },
  error => {
    return Promise.reject(error);
  }
);

// axios.interceptors.response.use(
//   response => {
//     return Promise.resolve(response);
//   },
//   function(error) {
//     if (error.response.status === 401) {
//       console.log("401 error");
//     }
//     return Promise.reject(error);
//   }
// );

export default biko;
