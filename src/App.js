import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import firebase from 'firebase/app';
import {FirebaseDatabaseProvider} from "@react-firebase/database";
import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Dashboard from "./layouts/Dashboard";
import Login from "./layouts/Login/Login";
import { SnackbarProvider } from "notistack";
import { createBrowserHistory } from "history";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import firebaseConfig from './config/firebase';

firebase.initializeApp(firebaseConfig);

const rootHistory = createBrowserHistory();

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#f57159',
      main: '#f5674d',
      dark: '#e35f47'
    }
  },
  overrides: {
    MuiFormControl: {
      root: {
        marginTop: 10
      }
    },
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before,&:before": {
          borderColor: "#D2D2D2 !important",
          borderWidth: "1px !important"
        },
      },
      focused: {}
    },
    MuiInputLabel: {
      root: {
        color: "#AAA !important",
        fontWeight: "400",
        fontSize: "14px",
        lineHeight: "1.42857"
      },
      focused: {
        color: "#AAA",
      }
    },
    // Style sheet name ⚛️
    MuiSelect: {
      root: {},
      icon: {
        top: 'calc(50% - 8px)'
      }
    },
  },
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <Router history={rootHistory}>
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/" component={Dashboard} />
            </Switch>
          </Router>
        </MuiPickersUtilsProvider>
      </SnackbarProvider>
    </ThemeProvider>
  )
};

export default App;
