import React from 'react';
import { withStyles } from "@material-ui/core";
import classNames from "classnames";
import styles from './styles';
import {format} from 'date-fns';
import ptBR from 'date-fns/locale/pt-BR';

const ChatBubble = ({sent = true, message, hour, classes, onlyEmoji}) => {
  return (
    <div className={classNames({
      [classes.container]: true
    })}>
      <div className={classNames({
        [classes.bubbleWrapper]: true,
        [classes.bubbleWrapperSent]: sent
      })}>

        {onlyEmoji ? (
          <span className={classNames({
            [classes.emoji]: true
          })}>{message}</span>
        ) : (
          <div className={classNames({
            [classes.balloon]: true,
            [classes.balloonSent]: sent,
          })}>
            <span className={classNames({
            [classes.balloonText]: true,
            [classes.balloonTextSent]: sent,
          })}>{message}</span>
          </div>
        )}
        <span className={classNames({
          [classes.hour]: true,
        })}>{format(hour, 'PPp', {locale: ptBR})}</span>
      </div>
    </div>
  );
};
export default withStyles(styles)(ChatBubble);
