import { primaryColor, secondaryLight, grayColor, black, whiteColor } from "assets/jss/biko-dashboard.jsx";

export default {
  container: {
    marginTop: 16,
    display: 'flex'
  },
  bubbleWrapper: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 0,
    marginRight: 'auto'
  },
  bubbleWrapperSent: {
    alignItems: 'flex-end',
    marginLeft: 'auto',
    marginRight: 0
  },
  balloon: {
    padding: 8,
    borderRadius: 16,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 16,
    backgroundColor: secondaryLight,
    maxWidth: 284
  },
  balloonSent: {
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 0,
    backgroundColor: primaryColor
  },
  balloonText: {
    fontSize: 14,
    color: black
  },
  balloonTextSent: {
    color: whiteColor
  },
  hour: {
    fontSize: 11,
    color: grayColor
  },
  hourSent: {
    color: whiteColor
  },
  emoji: {
    fontSize: 48,
    lineHeight: 1
  }
}
