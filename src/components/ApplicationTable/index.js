import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import {FiX} from "react-icons/fi";
import applicationTableStyle from "../../assets/jss/biko-dashboard/components/applicationTableStyle.jsx";
import { useHistory} from "react-router";

import NotFoundIllustration from "../../assets/img/not-found.svg"
import moment from "moment";

const filterApplicationFields = array => {
  return array.map(obj => {
    return Object.keys(obj)
      .map(key => {
        if (key === "createdAt" || key === "updatedAt")
          return moment(obj[key]).calendar();
        return obj[key];
      });
  });
};

function CustomTable({ ...props }) {
  const history = useHistory();
  const { classes, tableHead, tableData, tableHeaderColor } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
            <TableRow>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}

        {tableData.length > 0 ? (
          <TableBody>
            {filterApplicationFields(tableData).map((prop, key) => {
              return (
                <TableRow
                  key={key}
                  className={classes.tableRow}
                  onClick={() => {
                    history.push(`/applications/${prop[0]}`);
                  }}
                >
                  {prop.map((prop, key) => {
                    return (
                      <TableCell className={classes.tableCell} key={key}>
                        {prop}
                      </TableCell>
                    );
                  })}
                  <TableCell className={classes.tableActions}>
                    <Tooltip
                      id="tooltip-top-start"
                      title="Remove"
                      placement="top"
                      classes={{ tooltip: classes.tooltip }}
                    >
                      <IconButton
                        aria-label="Close"
                        className={classes.tableActionButton}
                      >
                        <FiX
                          className={
                            classes.tableActionButtonIcon + " " + classes.close
                          }
                        />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
          ) : (
            <TableRow>
              <TableCell className={classes.noApplications}>
                <img src={NotFoundIllustration} alt="" className={classes.noApplicationsIllustration}/>
                <h4>Não existem candidaturas para esta vaga</h4>
              </TableCell>
            </TableRow>
          )
        }
      </Table>
    </div>
  );
}

CustomTable.defaultProps = {
  tableHeaderColor: "gray"
};

CustomTable.propTypes = {
  history: PropTypes.object,
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

export default withStyles(applicationTableStyle)(CustomTable);
