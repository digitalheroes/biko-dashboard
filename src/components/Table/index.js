import { withStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import { FiX } from "react-icons/fi";
// core components
import styles from "../../assets/jss/biko-dashboard/components/tableStyle.jsx";

const useStyles = makeStyles(styles);

function CustomTable(props) {
  const classes = useStyles();
  const { tableHead, tableData, onClickRow } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead && (
          <TableHead>
            <TableRow className={classes.tableHeadRow}>
              {tableHead.map((prop, key) => (
                  <TableCell
                    className={[classes.tableCell, classes.tableHeadCell]}
                    key={key}>
                    {prop}
                  </TableCell>
                )
              )}
            </TableRow>
          </TableHead>
        )}
        <TableBody>
          {tableData.map((row, key) => (
              <TableRow key={key} className={classes.tableRow} onClick={() => {
                onClickRow(row[0])
              }}>
                {row.map((prop, key) => (
                    <TableCell className={classes.tableCell} key={key}>
                      {prop}
                    </TableCell>
                  )
                )}
                <TableCell className={classes.tableActions}>
                  <Tooltip
                    id="tooltip-top-start"
                    title="Deletar"
                    placement="top"
                    classes={{ tooltip: classes.tooltip }}
                  >
                    <Button
                      color="secondary"
                      aria-label="Close"
                      className={classes.tableActionButton}
                    >
                      <FiX
                        className={[classes.tableActionButtonIcon, classes.close]}
                      />
                    </Button>
                  </Tooltip>
                </TableCell>
              </TableRow>
            )
          )}
        </TableBody>
      </Table>
    </div>
  );
}

CustomTable.defaultProps = {
  tableHeaderColor: "gray"
};

CustomTable.propTypes = {
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

export default withStyles(styles)(CustomTable);

